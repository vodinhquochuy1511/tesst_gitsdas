<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_MODULES')) {
    exit('Stop!!!');
}

$sql_drop_module = [];

$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_them_moi_du_ans;';
$sql_drop_module[] = 'DROP TABLE IF EXISTS ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . '_tags;';

$sql_create_module = $sql_drop_module;

$sql_create_module[] = 'CREATE TABLE ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . "_them_moi_du_ans (
 id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
 ma_du_an varchar(250) NULL,
 ten_du_an varchar(250) NOT NULL,
 slug_ten_du_an varchar(250) NOT NULL,
 mo_ta_ngan text NOT NULL,
 mo_ta_chi_tiet text NOT NULL,
 hinh_anh varchar(250) NOT NULL,
 tien_quyen_gop double NOT NULL,
 thoi_han date NOT NULL,
 id_nguoi_tao int(11) NOT NULL,
 id_tag varchar(250) NOT NULL,
 is_duyet int(11) DEFAULT 3,
 is_open int(11) NOT NULL,
 created_at int(11),
 updated_at int(11),
 PRIMARY KEY (id),
 UNIQUE KEY slug_ten_du_an (slug_ten_du_an)
) ENGINE=MyISAM";

$sql_create_module[] = 'CREATE TABLE ' . $db_config['prefix'] . '_' . $lang . '_' . $module_data . "_tags (
    id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
    ten_tag varchar(250) NOT NULL,
    slug_ten_tag varchar(250) NOT NULL,
    created_at int(11),
    updated_at int(11),
    PRIMARY KEY (id),
    UNIQUE KEY slug_ten_tag (slug_ten_tag)
   ) ENGINE=MyISAM";

   

