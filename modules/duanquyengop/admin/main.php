<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}
include('project.php');
global $admin_info;
$page_title = $lang_module['main'];
$xtpl = new XTemplate('main.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);

if($nv_Request->isset_request('create', 'post,get')){
    $err = request($_POST, $array_name);
    if(!empty($err)){
        nv_jsonOutput([
            'status' => false,
            'err' => $err,
        ]);
    }else{
        $link_luu_anh = NV_ROOTDIR . '/' . NV_UPLOADS_DIR . '/' . $module_name;
        $list_anh = '';
        for($i = 0; $i < $_POST['so_luong_anh'];$i++){
            $upload = new NukeViet\Files\Upload('images', $global_config['forbid_extensions'], $global_config['forbid_mimes'], NV_UPLOAD_MAX_FILESIZE, NV_MAX_WIDTH, NV_MAX_HEIGHT);
            $upload->setLanguage($lang_global);
            $upload_info = $upload->save_file($_FILES['hinh_anh_'.$i], $link_luu_anh, false, $global_config['nv_auto_resize']);
            if(strlen($list_anh) > 10){
                $list_anh .= ',' . NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
            }else{
                $list_anh = NV_BASE_SITEURL . '/' . NV_UPLOADS_DIR . '/' .$module_name . '/' . $upload_info['basename'];
            }
        }
        $_POST['hinh_anh'] = $list_anh;
        $_POST['id_nguoi_tao'] = $admin_info['userid'];
        $_POST['slug_ten_du_an'] = createSlug($_POST['ten_du_an']);
        $_POST['created_at'] = NV_CURRENTTIME;
        $_POST['updated_at'] = 0;
        foreach ($duan as $key => $value) {
            $post[$value] = $_POST[$value];
        }
        $tag = explode(',', $_POST['id_tag']);
        $check = check($post['slug_ten_du_an'],'slug_ten_du_an',$tableDuan);
        if($check){
            $err[] = 'Ten du an da ton tai !';
            nv_jsonOutput([
                'status' => false,
                'err' => $err,
            ]);
        }else{
            $exe = store($post, $tableDuan);
            if($exe){
                foreach ($tag as $value) {
                    $check = check(createSlug($value), 'slug_ten_tag', $databaseTag);
                    if($check == null){
                        $sql ='INSERT INTO `nv4_vi_duanquyengop_tags`(`ten_tag`, `slug_ten_tag`, `created_at`, `updated_at`) 
                        VALUES (:ten_tag,:slug_ten_tag,:created_at,:updated_at)';
                        $stmt =$db->prepare($sql);
                        $stmt->bindParam('ten_tag', $value);
                        $stmt->bindParam('slug_ten_tag', createSlug($value));
                        $stmt->bindValue('created_at', NV_CURRENTTIME);
                        $stmt->bindValue('updated_at', 0);
                        $stmt->execute();
                    }
                }
                nv_jsonOutput([
                    'status' => true,
                ]);
            }     
        }
        
    }
}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
