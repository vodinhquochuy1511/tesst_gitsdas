<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}
$tableDuan = 'nv4_vi_duanquyengop_them_moi_du_ans';

$databaseTag = 'nv4_vi_duanquyengop_tags';

$array_name = [
    'ten_du_an'         => 'Ten du an',
    'mo_ta_ngan'        => 'Mo ta ngan',
    'mo_ta_chi_tiet'    => 'Mo ta chi tiet',
    'hinh_anh'          => 'Hinh anh',
    'tien_quyen_gop'    => 'Tien quyen gop',
    'thoi_han'          => 'Thoi han',
    'id_tag'            => 'Tag',
    'is_open'           => 'Trang thai',
    
];

$duan = [
    'ten_du_an',
    'slug_ten_du_an',
    'mo_ta_ngan',
    'mo_ta_chi_tiet',
    'hinh_anh',
    'tien_quyen_gop',
    'thoi_han',
    'id_nguoi_tao',
    'id_tag',
    'is_open',
    'created_at',
    'updated_at',
];

$duan_update = [
    'ten_du_an',
    'slug_ten_du_an',
    'mo_ta_ngan',
    'mo_ta_chi_tiet',
    'hinh_anh',
    'tien_quyen_gop',
    'thoi_han',
    'id_nguoi_tao',
    'id_tag',
    'is_open',
    'updated_at',
];



