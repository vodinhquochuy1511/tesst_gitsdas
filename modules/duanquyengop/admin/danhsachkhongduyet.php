<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}
include('project.php');
$page_title = $lang_module['danhsachkhongduyet'];

$sql = "SELECT nv4_vi_duanquyengop_them_moi_du_ans.* ,nv4_users.first_name 
        FROM nv4_vi_duanquyengop_them_moi_du_ans 
        LEFT JOIN nv4_users 
        ON nv4_vi_duanquyengop_them_moi_du_ans.id_nguoi_tao = nv4_users.userid 
        WHERE nv4_vi_duanquyengop_them_moi_du_ans.is_duyet = 2";
$res = $db->query($sql);
$list_ds_khong_duyet = $res->fetchAll();        

if($nv_Request->isset_request('chuyenveduan','post,get')){
    $_POST['is_duyet'] = 3;
    $update = update($_POST['id'], $_POST, $tableDuan);
    if($update){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}

if($nv_Request->isset_request('delete','post,get')){
    $sql = "DELETE FROM `nv4_vi_duanquyengop_them_moi_du_ans` WHERE id=" . $_POST['id'];
    $res = $db->query($sql);
    if($res){
        nv_jsonOutput([
            'status' => true,
        ]);
    }else{
        nv_jsonOutput([
            'status' => false,
        ]);
    }
}

$xtpl = new XTemplate('danhsachkhongduyet.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);

foreach($list_ds_khong_duyet as $key => $value){
    $value['hinh_anh'] = explode(',', $value['hinh_anh'])[0];
    $xtpl->assign('VALUE', $value);
    $xtpl->assign('KEY', $key + 1);
    $xtpl->parse('main.list_du_an');
}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
